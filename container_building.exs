alias Buildah.{Cmd, Pacman, Print}

pacman_catatonit_on_container = fn (container, options) ->
    Pacman.packages_no_cache(container, ["catatonit"], options)
    {catatonit_exec_, 0} = Cmd.run(container, ["sh", "-c", "command -v catatonit"])
    {_, 0} = Cmd.config(
        container,
        entrypoint: "[\"#{String.trim(catatonit_exec_)}\", \"--\"]"
    )
end

pacman_catatonit_test = fn (container, _image_ID, options) ->
    {_, 0} = Cmd.run(container, ["catatonit", "--version"], into: IO.stream(:stdio, :line))
    # {_, 0} = Podman.Cmd.run(image, ["catatonit", "--version"],
    #     tty: true, rm: true, into: IO.stream(:stdio, :line)
    # )
    Pacman.packages_no_cache(container, ["psmisc"], options)
    {_, 0} = Cmd.run(container, ["pstree"], into: IO.stream(:stdio, :line))
    # {_, 0} = Podman.Cmd.run(image, ["pstree"],
    #     tty: true, rm: true, into: IO.stream(:stdio, :line)
    # )
end


{whoami_, 0} = System.cmd("whoami", [])
"root" = String.trim(whoami_)
# System.fetch_env!("STORAGE_DRIVER")
# IO.puts(System.argv())
{_, 0} = Cmd.version(into: IO.stream(:stdio, :line))
{_, 0} = Cmd.info(into: IO.stream(:stdio, :line))

# build_image = "archlinux/base"
# build_image = System.fetch_env!("BUILD_IMAGE")
# IO.puts(build_image)

# from_image = "docker.io/" <> build_image
from_image = System.fetch_env!("FROM_IMAGE")
IO.puts(from_image)

# image = "localhost/" <> build_image
# image = System.fetch_env!("IMAGE")
# IO.puts(image)

# destination = "docker://#{System.fetch_env!("CI_REGISTRY_IMAGE")}/" <> build_image
destination = System.fetch_env!("IMAGE_DESTINATION")
IO.puts(destination)

Buildah.from_push(
    from_image,
    pacman_catatonit_on_container,
    pacman_catatonit_test,
    destination: destination,
    quiet: System.get_env("QUIET")
)

Print.images()
